import 'package:bloc_winners/model/user_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class UserState extends Equatable {} // must be abstract

// my initiale state
class UserLoadingState extends UserState {
  @override
  List<Object?> get props => [];

}

//données chargés de serveur //data loaded state
class UserLoadedState extends UserState {
  final List<UserModel> users;
  UserLoadedState(this.users);
  @override
  // la classe UserLoadedState a  des propriétés supplémentaires, donc la liste maintenant.
  List<Object?> get props => [users];
}

//loading error
class UserErrorState extends UserState {
  UserErrorState(this.error);
  final String error;
  @override
  // la classe UserLoadingState n'a pas de propriétés supplémentaires, donc la liste est vide.
  List<Object?> get props => [error];
}
