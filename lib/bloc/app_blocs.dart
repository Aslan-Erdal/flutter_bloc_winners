import 'package:bloc/bloc.dart';
import 'package:bloc_winners/bloc/app_event.dart';
import 'package:bloc_winners/bloc/app_state.dart';
import 'package:bloc_winners/repository/repositories.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository _userRepository;
 
  UserBloc(this._userRepository) : super(UserLoadingState()) {
    on<LoadUserEvent>((event, emit) async { // on: listen an event : and emit to publish an event
      emit(UserLoadingState());
 
      try {
        final users = await _userRepository.getUsers();
        print(users);
        emit(UserLoadedState(users)); //This method will update our state.
      } catch (e) {
        print('+++++Error: $e');
        emit(UserErrorState(e.toString())); //Otherwise will occcurs an error if data is not exists or others.
      }

    });
  }
}