import 'package:bloc_winners/bloc/app_blocs.dart';
import 'package:bloc_winners/bloc/app_event.dart';
import 'package:bloc_winners/bloc/app_state.dart';
import 'package:bloc_winners/elements/widget_bloc_body.dart';
import 'package:bloc_winners/model/user_model.dart';
import 'package:bloc_winners/repository/repositories.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});
 
  @override
  Widget build(BuildContext context) {
    return // MultiBlocProvider( // pour gérer plusieurs bloc de provider
      //providers: [
        BlocProvider<UserBloc>(
          create: (BuildContext context) => UserBloc(UserRepository()),
        //),
      //],
      child: Scaffold(
        appBar: AppBar(title: const Text('List de gagnants:')),
        body: blocBody(),
      ),
    );
  }
}